import React, { Component } from 'react';
import Game from './Game';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Game width={19} height={13} />
      </div>
    );
  }
}

export default App;
