import React, { Component } from 'react';
import Grid from './Grid';
import Driver from '../logic/Driver';
import './Game.css'

export default class Game extends Component {
    constructor(props) {
        super(props);

        this.state = {
            width: this.props.width,
            height: this.props.height
        };

        const driver = new Driver();
        this.state = driver.generateReducer(this.state);

        this.handleInputChange = this.handleInputChange.bind(this);
        this.onClickGenerate = this.onClickGenerate.bind(this);
        this.onClickSolve = this.onClickSolve.bind(this);
        this.onKeyUp = this.onKeyUp.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    handleInputChange(event) {
        const name = event.target.name;
        const value = event.target.value < 99 ? event.target.value : 99;

        this.setState({
            [name]: value
        })
    }

    onClickGenerate() {
        const driver = new Driver();
        const state = driver.generateReducer(this.state);

        this.setState(state);
    }

    onClickSolve() {
        const driver = new Driver();
        const state = driver.solveReducer(this.state);

        this.setState(state);
    }

    onKeyUp(event) {
        const driver = new Driver();
        const state = driver.keyUpReducer(this.state, event.key);

        this.setState(state);
    }

    onClick(event) {
        const driver = new Driver();
        const state = driver.keyUpReducer(this.state, event.target.value);

        this.setState(state);
    }

    componentDidMount() {
        window.addEventListener("keyup", this.onKeyUp);
    };

    componentWillUnmount() {
        window.removeEventListener("keyup", this.onKeyUp);
    }

    getAppropriateTitle(width, height) {
        if (width < 3 || height < 3) {
            return "No Maze at all";
        }

        const area = width * height;
        if (area > 2000) {
            return "Maze of Doom";
        }
        else if (area > 400) {
            return "Maze of Heroes";
        }
        else if (area > 100) {
            return "Maze of Champions";
        }
        else {
            return "Maze of Little Difficulty";
        }
    }

    render() {
        return (
            <div className='game'>
                <div className='controls'>
                    <h1>{this.getAppropriateTitle(this.state.width, this.state.height)}</h1>
                    <label>
                        Width
                        <input name='width' type='number' value={this.state.width} onChange={this.handleInputChange} />
                    </label>
                    <label>
                        Height
                        <input name='height' type='number' value={this.state.height} onChange={this.handleInputChange} />
                    </label>

                    <button onClick={this.onClickGenerate}>Generate</button>
                    <button onClick={this.onClickSolve}>Solve</button>
                </div>

                <div className='container'>
                    <div className='leftcolumn'>
                        <div>
                            <button onClick={this.onClick} value='w'>Up</button>
                        </div>
                        <div>
                            <button onClick={this.onClick} value='a'>Left</button>
                            <button onClick={this.onClick} value='s'>Down</button>
                            <button onClick={this.onClick} value='d'>Right</button>
                        </div>
                    </div>
                    <div className='rightcolumn'>
                        <Grid grid={this.state.grid} position={this.state.position} route={this.state.route} />
                    </div>
                    <div className='clear'></div>
                </div>

                <div className='scoreboard'>
                    <p>Score: {this.state.score}</p>
                </div>
            </div>
        );
    }
}
