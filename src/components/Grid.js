import React, { Component } from 'react';
import { WALL, PASSAGE, BREADCRUMB, ENTRANCE, EXIT, isWithinList } from '../logic/GridFunctions';
import Vector from '../logic/Vector';
import Square from './Square';
import wall from '../images/wall.png';
import man from '../images/man.png';
import breadcrumb from '../images/breadcrumb.png';
import './Grid.css'

export default class Grid extends Component {

    makeUniqueKey(x, y) {
        return x + ',' + y;
    }

    generateSquareType(type) {
        switch (type) {
            case WALL:
                return 'wall';
            case PASSAGE:
            case BREADCRUMB:
            case ENTRANCE:
            case EXIT:
                return 'passage';
            default:
                return 'square';
        }
    }

    generateCellContent(location) {
        if (this.props.grid[location.y][location.x].type === WALL) {
            return (
                <img src={wall} alt='' />
            );
        }
        else if (this.props.position != null && this.props.position.isSameAs(location)) {
            return (
                <img src={man} alt='you' />
            );
        }
        if (this.props.route != null && isWithinList(location, this.props.route)) {
            return (
                <img src={breadcrumb} alt='breadcrumb' />
            );
        }
        return '';
    }

    render() {
        if (this.props.grid === undefined || this.props.grid === null || this.props.grid.length === 0) return (
            <div>
            </div>
        );

        var rows = this.props.grid.map((row, y) => {
            var cells = row.map((cell, x) => {

                const location = new Vector(x, y);
                const key = this.makeUniqueKey(x, y);
                const cellType = cell.type;
                const content = this.generateCellContent(location);
                const squareType = this.generateSquareType(cellType);

                return (
                    <Square key={key} type={squareType} content={content} />
                );
            });

            return (
                <div className='row' key={y}>
                    {cells}
                </div>
            );
        });

        return (
            <div className='grid' style={{ width: this.props.grid[0].length * 29 }}>
                {rows}
            </div>
        );
    }
}
