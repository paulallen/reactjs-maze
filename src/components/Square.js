import React, { Component } from 'react';
import './Square.css'

// Stateless Square passed type and content
export default class Square extends Component {
    
    render() {
        return (
            <div className={this.props.type}>{this.props.content}</div>
        );
    }
}
