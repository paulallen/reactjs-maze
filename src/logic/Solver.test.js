import { PASSAGE, ENTRANCE, EXIT, createGrid, copyGrid, getNeighbours, countNeighbouringWalls } from './GridFunctions';
import Solver from './Solver';
import Vector from './Vector';

it('starts at current position', () => {

    const width = 7;
    const height = 11;

    const grid = createGrid(width, height);
    const position = new Vector(1, 1);
    const state = {
        grid: grid,
        position: position
    };

    const solver = new Solver();
    const newstate = solver.firstLocationReducer(state);

    expect(newstate.route).not.toEqual(null);
    expect(newstate.route).not.toEqual(undefined);
    expect(newstate.route.length).toEqual(1);
    expect(newstate.route[0].isSameAs(position)).toEqual(true);
});


it('selects a neighbouring location', () => {

    const width = 7;
    const height = 11;

    const grid = createGrid(width, height);
    grid[1][1].type = PASSAGE;
    grid[1][2].type = PASSAGE;

    const position = new Vector(1, 1);
    const route = [position];

    const state = {
        grid: grid,
        position: position,
        route: route
    };

    const solver = new Solver();
    const newstate = solver.stepAndBreadcrumbReducer(state, width, height);

    const expected = new Vector(2, 1);

    expect(newstate.route).not.toEqual(null);
    expect(newstate.route).not.toEqual(undefined);
    expect(newstate.route.length).toEqual(2);
    expect(newstate.route[1].isSameAs(expected)).toEqual(true);
});


it('backs up', () => {

    const width = 7;
    const height = 11;

    const grid = createGrid(width, height);
    grid[1][1].type = PASSAGE;
    grid[1][2].type = PASSAGE;

    const position = new Vector(1, 1);
    const next = new Vector(2, 1);
    const route = [position, next];

    const state = {
        grid: grid,
        position: position,
        route: route
    };

    const solver = new Solver();
    const newstate = solver.backupReducer(state, width, height);

    expect(newstate.route).not.toEqual(null);
    expect(newstate.route).not.toEqual(undefined);
    expect(newstate.route.length).toEqual(1);
    expect(newstate.route[0].isSameAs(position)).toEqual(true);
});

