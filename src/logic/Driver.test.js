import Driver from './Driver';
import Vector from './Vector';

it('handles w', () => {

    const driver = new Driver();
    const move = driver.determineMove('w');

    expect(move).not.toEqual(null);
    expect(move.isSameAs(new Vector(0, -1))).toEqual(true);
});

it('handles a', () => {

    const driver = new Driver();
    const move = driver.determineMove('a');

    expect(move).not.toEqual(null);
    expect(move.isSameAs(new Vector(-1, 0))).toEqual(true);
});

it('handles s', () => {

    const driver = new Driver();
    const move = driver.determineMove('s');

    expect(move).not.toEqual(null);
    expect(move.isSameAs(new Vector(0, 1))).toEqual(true);
});


it('handles d', () => {

    const driver = new Driver();
    const move = driver.determineMove('d');

    expect(move).not.toEqual(null);
    expect(move.isSameAs(new Vector(1, 0))).toEqual(true);
});

it('ignores q', () => {

    const driver = new Driver();
    const move = driver.determineMove('q');

    expect(move).toEqual(null);
});
