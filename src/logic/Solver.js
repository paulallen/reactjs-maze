import { BREADCRUMB, copyGrid, getNeighbours, isWithinBounds, isPassage, isExit } from './GridFunctions';

class Solver {

    // grid, route -> appropriate neighbouring location
    getNextUnvisitedLocation(state, width, height) {
        const location = state.route[state.route.length - 1];
        const neighbours = getNeighbours(location);
        for (var n = 0; n < neighbours.length; ++n) {
            const neighbour = neighbours[n];
            if (isWithinBounds(width, height, neighbour)) {
                if (isPassage(state.grid, neighbour) || isExit(state.grid, neighbour)) {
                    return neighbour;
                }
            }
        }
        return null;
    }

    // No route
    initializeReducer(state) {

        return Object.assign({},
            state, {
                route: null
            });
    }

    // grid, position -> grid, route
    firstLocationReducer(state) {
        const grid = copyGrid(state.grid);

        const location = state.position;
        grid[location.y][location.x].type = BREADCRUMB;

        var route = [];
        route.push(location);

        return Object.assign({},
            state, {
                grid: grid,
                route: route
            });
    }

    // grid, route -> grid, longer route
    stepAndBreadcrumbReducer(state, width, height) {
        const grid = copyGrid(state.grid);

        var location = state.route[state.route.length - 1];
        location = this.getNextUnvisitedLocation(state, width, height);
        grid[location.y][location.x].type = BREADCRUMB;

        var route = state.route.slice(0);
        route.push(location);

        return Object.assign({},
            state, {
                grid: grid,
                route: route
            });
    }

    // grid, route -> grid, shorter route
    backupReducer(state) {
        const route = state.route.slice(0, state.route.length - 1);

        return Object.assign({},
            state, {
                route: route
            });
    }

    // grid, position -> grid, route
    solveReducer(state, width, height) {

        if (state.grid === undefined || state.grid === null || state.grid.length === 0) {
            return;
        }

        if (state.route != null) {
            return;
        }

        const exit = state.exit;
        
        state = this.firstLocationReducer(state);

        while (!state.route[state.route.length - 1].isSameAs(exit)) {

            const location = this.getNextUnvisitedLocation(state, width, height);
            if (location != null) {
                // step and breadcrumb
                state = this.stepAndBreadcrumbReducer(state, width, height);
            }
            else if (state.route.length > 0) {
                // backtrack
                state = this.backupReducer(state);
            }
        }

        return state;
    }
}

export default Solver;
