// Immutable vector with {x, y}

class Vector {
    constructor(x, y) {
        this._x = x;
        this._y = y;
    }
    
    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }

    add(delta) {
        return new Vector( this._x + delta.x, this._y + delta.y);
    }

    isSameAs(vector) {
        return this.x === vector.x && this.y === vector.y;
    }

    toString() {
        return this._x + ',' + this._y;
    }
}

export default Vector;
