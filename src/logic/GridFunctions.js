import Vector from './Vector';

export const WALL = '?';
export const PASSAGE = '.';
export const ENTRANCE = 'E';
export const EXIT = 'X';
export const BREADCRUMB = '+';

// All WALL
export const createGrid = (width, height) => {

    var grid = new Array(height);
    for (var y = 0; y < height; ++y) {
        grid[y] = new Array(width);
        for (var x = 0; x < width; ++x) {
            grid[y][x] = { type: WALL }
        }
    }

    return grid;
}

// NB explicitly copies only type
export const copyGrid = (grid) => {

    var copy = [];
    for (var y = 0; y < grid.length; ++y) {
        copy[y] = grid[y].slice(0);
        for (var x = 0; x < copy[y].length; ++x) {
            copy[y][x] = {
                type: grid[y][x].type
            }
        }
    }

    return copy;
}

export const isWithinBounds = (width, height, location) => {
    return location.x > -1 && location.x < width &&
        location.y > -1 && location.y < height;
}

export const isOutOfBounds = (width, height, location) => {
    return !isWithinBounds(width, height, location);
}

export const isWithinExteriorWalls = (width, height, location) => {
    return location.x > 0 && location.x < width - 1 &&
        location.y > 0 && location.y < height - 1;
}

export const isWithinList = (location, list) => {
    for (var n = 0; n < list.length; ++n) {
        const segment = list[n];
        if (location.isSameAs(segment)) {
            return true;
        }
    }
    return false;
}

export const getNeighbours = (location) => {
    const neighbours = [
        new Vector(location.x + 1, location.y),
        new Vector(location.x - 1, location.y),
        new Vector(location.x, location.y - 1),
        new Vector(location.x, location.y + 1)
    ]
    return neighbours;
}

export const isWall = (grid, location) => {
    return grid[location.y][location.x].type === WALL;
}

export const isPassage = (grid, location) => {
    return grid[location.y][location.x].type === PASSAGE;
}

export const isExit = (grid, location) => {
    return grid[location.y][location.x].type === EXIT;
}

export const countNeighbouringWalls = (grid, width, height, location) => {
    var count = 0;
    var neighbours = getNeighbours(location);

    for (var n = 0; n < neighbours.length; ++n) {
        const neighbour = neighbours[n];
        if (isWall(grid, neighbour)) {
            ++count;
        }
    }

    return count;
}
