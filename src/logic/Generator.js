import { PASSAGE, ENTRANCE, EXIT, createGrid, copyGrid, getNeighbours, isWithinExteriorWalls, isWall, countNeighbouringWalls } from './GridFunctions';
import { shuffle } from './ArrayShuffle';
import Vector from './Vector';

class Generator {
    getRandomInteriorIndex = (length) => {
        return Math.round(1 + (length - 3) * Math.random());
    }

    getRandomInteriorLocation = (width, height) => {
        const y = this.getRandomInteriorIndex(height);
        const x = this.getRandomInteriorIndex(width);
        return new Vector(x, y);
    }

    getRandomPassageColumnIndex = (grid, x, height) => {
        var validYs = [];
        for (var y = 0; y < height; ++y) {
            if (grid[y][x].type === PASSAGE) {
                validYs.push(y);
            }
        }

        const index = Math.round((validYs.length - 1) * Math.random());
        return validYs[index];
    }

    // state.grid and state.tunnel -> new tunnel head
    chooseNextLocation = (state, width, height) => {

        const head = state.tunnel[state.tunnel.length - 1];

        var neighbours = getNeighbours(head);
        shuffle(neighbours);

        for (var n = 0; n < neighbours.length; ++n) {
            const neighbour = neighbours[n];

            if (isWithinExteriorWalls(width, height, neighbour)) {
                if (isWall(state.grid, neighbour)) {
                    if (countNeighbouringWalls(state.grid, width, height, neighbour) === 3) {

                        return neighbour;
                    }
                }
            }
        }

        return null;
    }

    // empty state -> grid, empty tunnel
    createGridReducer = (state, width, height) => {

        const grid = createGrid(width, height);

        return Object.assign({},
            state, {
                grid: grid,
                tunnel: []
            });
    }

    // grid, tunnel -> grid, longer tunnel
    tunnelReducer = (state, location) => {

        var grid = copyGrid(state.grid);

        grid[location.y][location.x] = { type: PASSAGE };

        var tunnel = state.tunnel.slice(0);
        tunnel.push(location);

        return Object.assign({},
            state, {
                grid: grid,
                tunnel: tunnel
            });
    }

    // grid, tunnel -> grid, shorter tunnel
    backupReducer = (state) => {

        const newtunnel = state.tunnel.slice(0, state.tunnel.length - 1);

        return Object.assign({},
            state, {
                tunnel: newtunnel
            });
    }

    // grid -> grid, entrance
    addEntranceReducer = (state, width, height) => {

        var grid = copyGrid(state.grid);
        const y = this.getRandomPassageColumnIndex(grid, 1, height);
        const entrance = new Vector(0, y);

        grid[entrance.y][entrance.x].type = ENTRANCE;

        return Object.assign({},
            state, {
                grid: grid,
                entrance: entrance
            });
    }

    // grid, entrance -> grid, entrance, exit
    addExitReducer = (state, width, height) => {

        var grid = copyGrid(state.grid);

        const y = this.getRandomPassageColumnIndex(grid, width - 2, height);
        const exit = new Vector(width - 1, y);

        grid[exit.y][exit.x].type = EXIT;

        return Object.assign({},
            state, {
                grid: grid,
                exit: exit
            });
    }

    // grid, entrance, exit -> grid, entrance, exit, position
    addInitialPositionReducer = (state, width, height) => {

        const position = new Vector(state.entrance.x, state.entrance.y);

        return Object.assign({},
            state, {
                position: position
            });
    }

    // state -> state.grid
    generateReducer = (state, width, height) => {

        // Not too small
        if (width < 3 || height < 3) {
            return state;
        }

        state = this.createGridReducer(state, width, height);

        const head = this.getRandomInteriorLocation(width, height);
        state = this.tunnelReducer(state, head);

        while (state.tunnel.length > 0) {
            const next = this.chooseNextLocation(state, width, height);
            if (next != null) {
                // tunnel further
                state = this.tunnelReducer(state, next);
            }
            else if (state.tunnel.length > 0) {
                // Back up
                state = this.backupReducer(state);
            }
        }

        state = this.addEntranceReducer(state, width, height);
        state = this.addExitReducer(state, width, height);
        state = this.addInitialPositionReducer(state, width, height);

        return state;
    }
}

export default Generator;
