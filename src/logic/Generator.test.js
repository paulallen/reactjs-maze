import { PASSAGE, ENTRANCE, EXIT, createGrid, copyGrid, getNeighbours, countNeighbouringWalls } from './GridFunctions';
import Generator from './Generator';
import Vector from './Vector';

it('selects a tunnel start location', () => {

    const generator = new Generator();
    const location = generator.getRandomInteriorLocation(3, 3);

    expect(location).not.toEqual(null);
    expect(location.y).toEqual(1);
    expect(location.x).toEqual(1);
});

it('selects a valid tunnel start location', () => {

    const width = 9;
    const height = 5;

    const generator = new Generator();
    const location = generator.getRandomInteriorLocation(width, height);

    expect(location).not.toEqual(null);
    expect(location.x).toBeGreaterThan(0);
    expect(location.x).toBeLessThan(width -1);
    expect(location.y).toBeGreaterThan(0);
    expect(location.y).toBeLessThan(height -1);
});

it('selects a new tunnel head location', () => {

    const width = 9;
    const height = 5;

    const grid = createGrid(width, height);
    const head = new Vector(1, 1);
    const tunnel = [head];
    grid[head.y][head.x].type = PASSAGE;

    var state = {
        grid: grid,
        tunnel: tunnel
    };

    const generator = new Generator();
    const location = generator.chooseNextLocation(state, width, height);

    expect(location).not.toEqual(null);
    expect(location.x).toBeGreaterThan(0);
    expect(location.x).toBeLessThan(8);
    expect(location.y).toBeGreaterThan(0);
    expect(location.y).toBeLessThan(4);
});

it('creates new state with grid and empty tunnel', () => {

    const width = 9;
    const height = 5;

    const state = {};
   
    const generator = new Generator();
    const newstate = generator.createGridReducer(state, width, height);

    expect(newstate.grid).not.toEqual(null);
    expect(newstate.grid.length).toEqual(height);
    expect(newstate.grid[0].length).toEqual(width);

    expect(newstate.tunnel).not.toEqual(undefined);
    expect(newstate.tunnel).not.toEqual(null);
    expect(newstate.tunnel.length).toEqual(0);
});

it('carves state with grid and longer tunnel', () => {

    const width = 9;
    const height = 5;

    const grid = createGrid(width, height);
    const head = new Vector(1, 1);
    const tunnel = [head];
    grid[head.y][head.x].type = PASSAGE;

    var state = {
        grid: grid,
        tunnel: tunnel
    };

    const generator = new Generator();
    const newstate = generator.tunnelReducer(state, head);

    expect(newstate.grid).not.toEqual(null);
    expect(newstate.tunnel).not.toEqual(null);
    expect(newstate.tunnel).not.toEqual(undefined);
    expect(newstate.tunnel.length).toEqual(2);
});

it('backs up to state with grid and shorter tunnel', () => {

    const width = 9;
    const height = 5;

    const grid = createGrid(width, height);
    const head = new Vector(1, 1);
    const next = new Vector(1, 2);
    const tunnel = [head, next];
    grid[head.y][head.x].type = PASSAGE;
    grid[next.y][next.x].type = PASSAGE;

    var state = {
        grid: grid,
        tunnel: tunnel
    };

    const generator = new Generator();
    const newstate = generator.backupReducer(state, head);

    expect(newstate.grid).not.toEqual(null);
    expect(newstate.tunnel).not.toEqual(null);
    expect(newstate.tunnel).not.toEqual(undefined);
    expect(newstate.tunnel.length).toEqual(1);
});

it('adds entrance', () => {

    const width = 9;
    const height = 5;

    var grid = createGrid(width, height);
    grid[2][1].type = PASSAGE;
    var state = {
        grid: grid
    };

    const generator = new Generator();
    const newstate = generator.addEntranceReducer(state, 19, 5);

    expect(newstate.entrance).not.toEqual(null);
    expect(newstate.entrance).not.toEqual(undefined);
});

it('adds exit', () => {

    const width = 9;
    const height = 5;

    var grid = createGrid(width, height);
    grid[4][width -2].type = PASSAGE;
    const entrance = new Vector(0, 2);
    
    var state = {
        grid: grid,
        entrance: entrance
    };

    const generator = new Generator();
    const newstate = generator.addExitReducer(state, width, height);

    expect(newstate.entrance).not.toEqual(null);
    expect(newstate.entrance).not.toEqual(undefined);

    expect(newstate.exit).not.toEqual(null);
    expect(newstate.exit).not.toEqual(undefined);
});


it('adds position', () => {

    const width = 9;
    const height = 5;

    var grid = createGrid(width, height);
    const entrance = new Vector(0, 2);
    const exit = new Vector(18, 3);
    var state = {
        grid: grid,
        entrance: entrance,
        exit: exit
    };

    const generator = new Generator();
    const newstate = generator.addInitialPositionReducer(state, width, height);

    expect(newstate.entrance).not.toEqual(null);
    expect(newstate.entrance).not.toEqual(undefined);

    expect(newstate.exit).not.toEqual(null);
    expect(newstate.exit).not.toEqual(undefined);

    expect(newstate.position).not.toEqual(null);
    expect(newstate.position).not.toEqual(undefined);
});
