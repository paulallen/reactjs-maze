import { WALL, PASSAGE, createGrid, copyGrid, getNeighbours, countNeighbouringWalls } from './GridFunctions';

it('creates a lot of walls', () => {

    const grid = createGrid(7, 4);

    expect(grid.length).toEqual(4);
    expect(grid[0].length).toEqual(7);

    for (var y = 0; y < grid.length; ++y) {
        for (var x = 0; x < grid[0].length; ++x) {
            expect(grid[y][x].type).toEqual(WALL);
        }
    }
});

it('copies grid types', () => {

    const grid = createGrid(6, 17);
    grid[1][1].type = PASSAGE;

    const copy = copyGrid(grid);

    expect(copy.length).toEqual(grid.length);
    expect(copy[0].length).toEqual(grid[0].length);

    for (var y = 0; y < grid.length; ++y) {
        for (var x = 0; x < grid[0].length; ++x) {
            expect(copy[y][x].type).toEqual(grid[y][x].type);
        }
    }
});
