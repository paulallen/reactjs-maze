import { isOutOfBounds, isWall } from './GridFunctions';
import Vector from './Vector';
import Generator from './Generator';
import Solver from './Solver';

class Driver {

    // initialize state, including Generator and Solver
    generateReducer = (state) => {

        const generator = new Generator();
        state = generator.generateReducer(state, state.width, state.height)

        const solver = new Solver();
        state = solver.initializeReducer(state);

        return Object.assign({},
            state,
            {
                steps: 0,
                score: ''
            });
    }

    solveReducer = (state) => {
        const solver = new Solver();
        state = solver.solveReducer(state, state.width, state.height);

        return state;
    }

    // state -> position, steps, score if appropriate
    keyUpReducer(state, key) {

        if (!state.position) {
            return;
        }

        if (state.position.isSameAs(state.exit)) {
            return;
        }

        const move = this.determineMove(key);
        if (move === null) {
            return;
        }

        const newposition = state.position.add(move);
        if (isOutOfBounds(state.width, state.height, newposition)) {
            return;
        }

        if (isWall(state.grid, newposition)) {
            return;
        }

        const steps = state.steps + 1;

        var score = '';

        if (newposition.isSameAs(state.exit)) {
            if (state.route === null) {
                score = state.width * state.height - steps;
            }
            else {
                score = 'nada';
            }
        }

        return Object.assign({},
            state, {
            position: newposition,
            steps: steps,
            score: score
        });        
    }

    determineMove = (key) => {
        switch (key) {
            case 'w':
            case 'ArrowUp':
                return new Vector(0, -1);
            case 'a':
            case 'ArrowLeft':
                return new Vector(-1, 0);
            case 's':
            case 'ArrowDown':
                return new Vector(0, 1);
            case 'd':
            case 'ArrowRight':
                return new Vector(1, 0);
            default:
                return null;
        }
    }
}

export default Driver;
