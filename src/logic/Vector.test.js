import Vector from './Vector';

it('constructs a new vector', () => {
    const vector = new Vector(1, 2);

    expect(vector.x).toEqual(1);
    expect(vector.y).toEqual(2);
});

it('adds vectors correctly', () => {
    const vector = new Vector(1, 2);
    const delta = new Vector(1, 1);

    const result = vector.add(delta);

    expect(result.x).toEqual(2);
    expect(result.y).toEqual(3);
});

it('compares different vectors correctly', () => {
    const vector = new Vector(1, 2);
    const another = new Vector(1, 1);

    const isSame = vector.isSameAs(another);

    expect(isSame).toEqual(false);
});

it('compares identical vectors correctly', () => {
    const vector = new Vector(1, 2);
    const another = new Vector(1, 2);

    const isSame = vector.isSameAs(another);

    expect(isSame).toEqual(true);
});
